package com.sorsix.imagesizecompressor;

import static com.sorsix.imagesizecompressor.utils.ImageCompressor.compressLosslessJpg;
import static com.sorsix.imagesizecompressor.utils.ImageCompressor.compressLossy;
import static com.sorsix.imagesizecompressor.utils.ImageCompressor.compressLossyPng;
import static com.sorsix.imagesizecompressor.utils.ImageConvertor.pngToJpg;

enum CompressionMode {
    KEEP_ORIGINAL_FORMAT,
    CONVERT_TO_JPG
}


public class ImageSizeCompressor {

    private static CompressionMode compressionMode = CompressionMode.KEEP_ORIGINAL_FORMAT;

    public static void main(String[] args) {
        String srcDir = "/home/martin/Desktop/imgs";
        String destDir = "/home/martin/Desktop/repl";
        FileWalker fw = new FileWalker(srcDir, destDir);


        // For PNG images, the end file size would be smaller if first converted to JPG and then compressed.
        // However if you want to keep image format (png), then use compressLossyPng method

        fw.walk((file, dest) -> {
            String inPath = file.getAbsolutePath();
            String outPath = dest.getAbsolutePath() + "/" + file.getName();
            String imageFormat = getFileExtension(file.getName());

            if (compressionMode.equals(CompressionMode.KEEP_ORIGINAL_FORMAT)) {
                if (imageFormat.equalsIgnoreCase("jpg")) {
                    compressLosslessJpg(inPath, outPath);
                    compressLossy(outPath, outPath, 80);
                } else if (imageFormat.equalsIgnoreCase("png")) {
                    compressLossyPng(inPath, outPath);
                }
            } else {
                // If converting all result images to JPG is acceptable
                if (imageFormat.equalsIgnoreCase("png")) {
                    String extensionRegex = "\\.[0-9a-z]+$";
                    outPath = outPath.replaceAll(extensionRegex, ".jpg");
                    pngToJpg(inPath, outPath);
                    inPath = outPath;
                }
                compressLosslessJpg(inPath, outPath);
                compressLossy(inPath, outPath, 80);
            }
        });
    }

    private static String getFileExtension(String path) {
        return path.substring(path.lastIndexOf('.') + 1, path.length());
    }
}
