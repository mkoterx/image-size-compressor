package com.sorsix.imagesizecompressor;

import java.io.File;
import java.util.function.BiConsumer;

public class FileWalker {

    private final String srcFolder;
    private final String destFolder;

    // TODO implement ingoring files
    public FileWalker(String srcDir, String destDir) {
        if (!new File(srcDir).exists())
            throw new RuntimeException(String.format("Source folder does not exist [%s]", srcDir));
        this.srcFolder = srcDir;
        this.destFolder = destDir;
    }

    /**
     *
     * @param fileConsumer where the first param is the file to be consumed and the second is param
     *                     is the destination folder
     */
    public void walk(BiConsumer<File, File> fileConsumer) {
        walk(srcFolder, fileConsumer);
    }

    private void walk(String path, BiConsumer<File, File> fileConsumer) {
        File root = new File(path);
        File[] list = root.listFiles();

        if (list == null) return;

        for (File f : list) {
            if (f.isDirectory()) {
                String dirPath = f.getAbsoluteFile().toString().replace(srcFolder, destFolder);
                new File(dirPath).mkdirs();
                walk(f.getAbsolutePath(), fileConsumer);
            }
            else {
                String destDir = f.getParent().replace(srcFolder, destFolder);
                fileConsumer.accept(f, new File(destDir));
            }
        }
    }
}