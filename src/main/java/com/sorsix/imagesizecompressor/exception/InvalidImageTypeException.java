package com.sorsix.imagesizecompressor.exception;

public class InvalidImageTypeException extends RuntimeException {

    public InvalidImageTypeException() {
        super();
    }

    public InvalidImageTypeException(String message) {
        super(message);
    }
}
