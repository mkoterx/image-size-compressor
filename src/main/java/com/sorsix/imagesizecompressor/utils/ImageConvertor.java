package com.sorsix.imagesizecompressor.utils;

import com.sorsix.imagesizecompressor.exception.ClientException;
import com.sorsix.imagesizecompressor.exception.InvalidImageTypeException;
import com.sorsix.imagesizecompressor.exception.ServerException;
import org.im4java.core.ConvertCmd;
import org.im4java.core.IM4JavaException;
import org.im4java.core.IMOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.IOException;

public final class ImageConvertor {

    private static final Logger logger = LoggerFactory.getLogger(ImageConvertor.class);

    private static final String DEFAULT_COLOR = "white";

    public static void pngToJpg(String inPath, String outPath) {
        pngToJpg(inPath, outPath, DEFAULT_COLOR);
    }

    public static void pngToJpg(String inPath, String outPath, String backgroundColor) {
        if (!outPath.endsWith(".jpg") && !outPath.endsWith(".jpeg"))
            throw new InvalidImageTypeException("Dest file must be jpg");
        IMOperation op = new IMOperation();
        op.addImage(inPath);
        op.background(backgroundColor);
        op.flatten();
        op.addImage(outPath);
        ConvertCmd cmd = new ConvertCmd();
        try {
            cmd.run(op);
            logger.info("Successfully converted png to jpg, image [{}]", inPath);
        } catch (IOException | InterruptedException e) {
            logger.error("Failed to convert png to jpg, image [{}], message: {}", inPath, e.getMessage());
            throw new ServerException(e.getMessage());
        } catch (IM4JavaException e) {
            logger.error("Failed to convert png to jpg, image [{}], message: {}", inPath, e.getMessage());
            throw new ClientException(e.getMessage());
        }
    }

    public static boolean jpgToPng() {
        throw new NotImplementedException();
    }
}
