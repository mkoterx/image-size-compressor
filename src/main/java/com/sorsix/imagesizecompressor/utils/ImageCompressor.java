package com.sorsix.imagesizecompressor.utils;

import com.sorsix.imagesizecompressor.exception.InvalidImageTypeException;
import org.apache.commons.io.FileUtils;
import org.im4java.core.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zeroturnaround.exec.ProcessExecutor;
import org.zeroturnaround.exec.stream.slf4j.Slf4jStream;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public final class ImageCompressor {

    //TODO implement the strategy pattern for this class

    private static final Logger logger = LoggerFactory.getLogger(ImageCompressor.class);

    private static int DEFAULT_QUALITY = 80;
    private static int DEFAULT_TIMEOUT = 5 * 1000;

    public static void compressLosslessJpg(String inPath, String outPath) {
        compressLosslessJpg(inPath, outPath, DEFAULT_TIMEOUT);
    }

    public static void compressLosslessJpg(String inPath, String outPath, long timeoutMillis) {
        String imageType = getFileExtension(inPath);
        if (!"jpg".equalsIgnoreCase(imageType) && !"jpeg".equalsIgnoreCase(imageType))
            throw new InvalidImageTypeException("input image must be of type jpg/jpeg");
        try {
            boolean override = false;
            if (inPath.equals(outPath)) { // jpegtran doesn't support the same location for input and output files
                outPath = outPath.replace("." + imageType, "tmp." + imageType);
                override = true;
            }

            new ProcessExecutor()
                    .command("jpegtran", "-copy", "none", "-progressive", "-outfile", outPath, inPath)
                    .timeout(timeoutMillis, TimeUnit.MILLISECONDS)
                    .redirectOutput(Slf4jStream.of(ImageCompressor.class).asInfo())
                    .redirectError(Slf4jStream.of(ImageCompressor.class).asError())
                    .execute();

            if (override) {
                File oldImage = new File(inPath);
                File newImage = new File(outPath);
                oldImage.delete();
                newImage.renameTo(oldImage);
            }
            logger.info("Compression (Lossless) success, image file [{}]", inPath);
        } catch (IOException | InterruptedException | TimeoutException e) {
            logger.error("Compression (Lossless) failed for image file [{}], message: {}", inPath, e.getMessage());
        }
    }

    public static void compressLossy(String inPath, String outPath) {
        compressLossy(inPath, outPath, DEFAULT_QUALITY);
    }

    /**
     * Can be used both for JPG or PNG images, but for compressing PNG images the method
     * {@link #compressLossyPng(String, String)} should be preferred
     * @param inPath
     * @param outPath
     * @param quality
     */
    public static void compressLossy(String inPath, String outPath, double quality) {
        if (!checkValidQuality(quality)) throw new IllegalArgumentException("quality value must be in range [0, 100]");
        if (!inPath.equals(outPath)) {
            try {
                FileUtils.copyFile(new File(inPath), new File(outPath));
            } catch (IOException e) {
                logger.error("Compression (Lossy) failed for image file [{}], message: {}", inPath, e.getMessage());
                return;
            }
        }
        GMOperation op = new GMOperation();
        op.addImage(outPath);
        op.quality(quality);
        executeLossyCompressionWithMogrify(outPath, op);
    }

    public static void compressLossy(String inPath, String outPath, double quality, int resizePercent) {
        if (!checkValidQuality(quality)) throw new IllegalArgumentException("quality must be in range [0, 100]");
        if (!checkValidResizePercent(quality))
            throw new IllegalArgumentException("resize percent must be in range [0, 100]");
        GMOperation op = new GMOperation();
        op.addImage(inPath);
        op.resize(resizePercent);
        op.quality(quality);
        op.interlace("Line");
        op.addImage(outPath);
        executeLossyCompressionWithConvert(inPath, op);
    }

    /**
     * Preferred method for PNG images
     * https://pngquant.org/install.html
     * https://pngquant.org/
     * @param srcPath
     * @param destPath
     */
    public static void compressLossyPng(String srcPath, String destPath) {
        compressLossyPng(srcPath, destPath, DEFAULT_QUALITY);
    }

    public static void compressLossyPng(String srcPath, String destPath, int quality) {
        String imageType = getFileExtension(srcPath);
        if (!"png".equalsIgnoreCase(imageType))
            throw new InvalidImageTypeException("input image must be of type PNG");
        try {
            new ProcessExecutor()
                    .command("pngquant", "--quality=" + quality, "--ext", "-tmp.png", srcPath)
                    .redirectOutput(Slf4jStream.of(ImageCompressor.class).asInfo())
                    .redirectError(Slf4jStream.of(ImageCompressor.class).asError())
                    .execute();
            File src = new File(srcPath.replace(".png", "-tmp.png"));
            File dest = new File(destPath);
            FileUtils.moveFile(src, dest);
            logger.info("Compression (Lossy) success, image [{}]", srcPath);
        } catch (IOException | InterruptedException | TimeoutException e) {
            logger.error("Compression (Lossy) failed, image [{}], message: {}", destPath, e.getMessage());
        }
    }

    private static void executeLossyCompressionWithMogrify(String inPath, Operation op) {
        try {
            boolean shouldUseGraphicMagic = true;
            MogrifyCmd cmd = new MogrifyCmd(shouldUseGraphicMagic);
            cmd.run(op);
            logger.info("Compression (Lossy) success, image [{}]", inPath);
        } catch (InterruptedException | IOException | IM4JavaException e) {
            logger.error("Compression (Lossy) failed, image [{}], message: {}", inPath, e.getMessage());
        }
    }

    private static void executeLossyCompressionWithConvert(String inPath, Operation op) {
        try {
            boolean shouldUseGraphicMagic = true;
            ConvertCmd cmd = new ConvertCmd(shouldUseGraphicMagic);
            cmd.run(op);
            logger.info("Compression (Lossy) success, image [{}]", inPath);
        } catch (InterruptedException | IOException | IM4JavaException e) {
            logger.error("Compression (Lossy) failed, image [{}], message: {}", inPath, e.getMessage());
        }
    }

    private static boolean checkValidQuality(double val) {
        return val >= 0.0 && val <= 100.0;
    }

    private static boolean checkValidResizePercent(double val) {
        return val >= 0.0 && val <= 100.0;
    }

    private static String getFileExtension(String path) {
        return path.substring(path.lastIndexOf('.') + 1, path.length());
    }
}

